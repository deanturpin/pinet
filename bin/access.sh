#!/bin/bash

tail -f /var/log/nginx/access.log | while read line; do
	ip=$(echo $line | cut -d ' ' -f1)
	echo -e "$line\n $(whois $ip | grep netname)" | lolcat --truecolor --animate
done

