from pymongo import MongoClient
import os
import magic

client = MongoClient("localhost", 27017)
db = client.pinet
collection = db["status"]

path = "/tmp/pinet/"
print(f"Searching in directory: {path}")

for root, _, files in os.walk(path):

    for status_file in files:
        full_path = root + status_file
        print(full_path)

        if "text" not in magic.from_file(full_path):
            print(full_path, "not a text file")
            os.remove(full_path)
            continue

        fd = open(full_path, 'r')
        lines = fd.readlines()

        # Parse config
        config = {}
        for line in lines:
            key, value = line.strip().split("=")
            config[key] = value

        # Check if we have at least a hostname
        key = "HOSTNAME"
        if not key in config:
            print("no HOSTNAME key, skipping")
            continue

        # Create or update record
        unique_id = config['EXTERNAL_IP'] + '-' + config['INTERNAL_IP']
        record_id1 = collection.update_one({"UNIQUE_ID" : unique_id},
                {'$set': config}, upsert=True)

        print("record id", record_id1)

        # And tidy up
        os.remove(full_path)

