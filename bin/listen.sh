#!/bin/bash

readonly status_dir=/tmp/pinet

if [[ ! -e $status_dir ]]; then
	echo Create status dir: $status_dir
	mkdir -p /tmp/pinet
else
	echo Status dir $status_dir already exists
fi

echo Wait for connections
# count=0
while :; do
	status_file=$(mktemp)
	#status_file=$count_$(mktemp)
	echo "EPOCH=$EPOCHSECONDS" > $status_file
	nc -lnvp 3389 >> $status_file
	echo Written to $status_file
	mv $status_file $status_dir
done

