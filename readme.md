# PiNet - aka PIN
- [Domain](domain.md)
- [Server](server.md)
- [Client](client.md)
- [Database](database.md) -- [crib sheet](crib.md)

# Network topology
```mermaid
graph LR
    client1 --- server
    client2 --- server
    client3 --- server
    client4 --- server

    server --- database[(DB)]

    database --- gui
```

