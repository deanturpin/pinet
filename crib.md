# Mongo crib sheet
## Connecting a client
Connect to the `pinet` database.
```bash
mongo pinet
```

```mongodb
# Which database am I using?
db

# All users
show users

# All databases
show databases
show dbs

show collections

# Switch to a database
use pinet

# Dump version -- actually reveals current database
db.system.version

```

## Inserting and finding
```mongodb
> db.users.insert({name:"seal", id:";sdfsd"})
WriteResult({ "nInserted" : 1 })

> db.users.find()
{ "_id" : ObjectId("60dd78286971ac9d951b0dea"), "name" : "seal", "id" : ";sdfsd" }

# List all records
db.status.find()

# Status
db.stats()
```

