# Google Cloud instance
- Create `f1-micro (1 vCPU, 0.6 GB memory)`
- $0.02 USD/hour for f1-micro and g1-small machine types

# Certificates
- LetsEncrypt

# Running client-specific scripts
Add to home directory, all scripts run and transmitted using the client header

# Deploy
- Via Docker?

