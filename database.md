Using MongoDB on a Debian 9 VM (10 is a bit more complicated).

Periodically purge database of old entries (or mark as old), and revoke access token.

```bash
$ sudo apt update
$ sudo apt install mongodb
$ sudo systemctl status mongodb
● mongodb.service - An object/document-oriented database
   Loaded: loaded (/lib/systemd/system/mongodb.service; enabled; vendor preset: enabled)
   Active: active (running) since Wed 2021-06-30 12:42:06 UTC; 2min 12s ago
     Docs: man:mongod(1)
 Main PID: 1682 (mongod)
   CGroup: /system.slice/mongodb.service
           └─1682 /usr/bin/mongod --unixSocketPrefix=/run/mongodb --config /etc/mongodb.conf

Jun 30 12:42:06 mongodb systemd[1]: Started An object/document-oriented database.

$ mongod --version
db version v3.2.11
git version: 009580ad490190ba33d1c6253ebd8d91808923e4
OpenSSL version: OpenSSL 1.0.2u  20 Dec 2019
allocator: tcmalloc
modules: none
build environment:
    distarch: x86_64
    target_arch: x86_64

```

## Check the port is open
```bash
ss -a | grep mongo
u_str  LISTEN     0      128    /run/mongodb/mongodb-27017.sock 11170                 * 0     
```

## Enable authentication
Set in `/etc/mongo.conf`.
```bash
# Turn on/off security.  Off is currently the default
#noauth = true
auth = true
```

Restart the service.
```bash
sudo systemctl restart mongodb
```

And now we can connect but not do anything.
```mongo
> db.users.find()
Error: error: {
        "ok" : 0,
        "errmsg" : "not authorized on test to execute command { find: \"users\", filter: {} }",
        "code" : 13
}
```

# Server config
- Add `default-allow-mongo` to open up the firewall.

# Accessing the database
```
python -m pip install pymongo
import pymongo
```

# Ubuntu 21
```
# Add the official repo
wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.4.list

# Install MongoDB and Python library
sudo apt update
sudo apt install python3-pymongo mongodb-org

# Enable and start MongoDB
sudo systemctl enable mongod
sudo systemctl start mongod

# Check the service is listening
ss -a | grep mongo
```

To only install the shell
```
sudo apt install mongodb-org-shell
mongo --version
MongoDB shell version v4.4.6
Build Info: {
    "version": "4.4.6",
    "gitVersion": "72e66213c2c3eab37d9358d5e78ad7f5c1d0d0d7",
    "openSSLVersion": "OpenSSL 1.1.1j  16 Feb 2021",
    "modules": [],
    "allocator": "tcmalloc",
    "environment": {
        "distmod": "ubuntu2004",
        "distarch": "x86_64",
        "target_arch": "x86_64"
    }
}
```

# References
- https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
- https://en.wikipedia.org/wiki/MongoDB
- https://wiki.crowncloud.net/?How_to_Install_MongoDB_on_Ubuntu_21_04

