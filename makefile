all: status

c: connect
connect:
	curl turpin.cloud | bash

deps:
	sudo apt install --yes git lm-sensors vim iputils-ping cron netcat

deps-server:
	sudo apt install cron nginx tree whois lolcat

deps-db:
	python3-magic mongodb-org python3-pymongo python3

a: access
access:
	bin/access.sh

auth:
	nc -klnvp 3389

p: process
process:
	python3 bin/process.py

listen:
	bin/listen.sh

s: simple
simple:
	tail -f /var/log/nginx/access.log

db:
	mongo pinet

status:
	watch --differences --interval .5 --color --no-title "date --utc; python3 bin/status.py; ls -lrt /tmp/pinet"

