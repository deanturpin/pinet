# curl turpin.cloud | bash

Exploring creating networks of Raspberry Pis that live on inaccessible private
networks, using Google Cloud as a centralised web server.

Also estimate power usage of system.

# Configuring a client
- Install Raspbian LITE 32-bit on a pi
- Copy in wpa_supplicant.conf and a blank file named `ssh`

## nginx
- Enable http and https in Google Cloud VM config
- If you hit the IP in a browser you should see the nginx default page
- Default HTML file is /var/www/html/index.html
- Identifying a client: http://www.cgi101.com/book/ch3/text.html
- git clone https://gitlab.com/deanturpin/fcluster /tmp/fcluster
- Copy index.html to web root
- Open up a port in the firewall: 	default-allow-rdp
- Start listening: nc -knvlp 3389 
- Reverse shell:  bash -i >& /dev/tcp/34.89.13.110/3389 0>&1

## Authentication
- Estbalish connection over tls
- Server manages list of access tokens
- Token written to disk when configuring SD card for first time
- Server issues commands rather than scripts: marionette

### Alternatively...
- Write own encryption tool

# Secure netcat
- https://superuser.com/questions/346958/can-the-telnet-or-netcat-clients-communicate-over-ssl#346962
- https://rietta.com/blog/openssl-generating-rsa-key-from-command/
- https://linux.101hacks.com/unix/gpg-command-examples/
- https://jameshfisher.com/2017/03/21/openssl-public-key-encryption/

# TLS wrappers
- https://www.stunnel.org/examples.html
- https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-using_stunnel
- hitch

# DiscOS
- Send udp disco packet
- `echo Sending DiscOvery packet`
- `cat $SUMMARY | nc -u 192.168.0.240 19876`

# File hashing
- Quick hash
- How to hash very large files?

# openssl
```
# Generate private key
openssl genrsa -out private.pem 4096

# Generate public key and copy to client
openssl rsa -pubout -in private.pem -out public.pem
cat public.pem

# Generate status
echo $EPOCHREALTIME > status.txt

# Encrypt small status file with public key
openssl rsautl -encrypt -in status.txt -out status.dat -inkey public.pem -pubin

# Decrypt with private key
openssl rsautl -decrypt -in status.dat -inkey private.pem
```

```
while :; do nc -l 3389 > ciphertext; echo -e "\nlocal time  $EPOCHSECONDS"; openssl rsautl -decrypt -in ciphertext -inkey private.pem; done

while :; do echo -e "remote time $EPOCHSECONDS\n$(ip route)" > status; cat status; openssl rsautl -encrypt -in status -out ciphertext -inkey public.pem -pubin; cat ciphertext > /dev/tcp/turpin.cloud/3389; sleep 1; done
```

# References
- https://www.revshells.com/

