# Connecting to the network
Deploy by fetching a script from the server.
```bash
curl turpin.cloud | bash
```

Can be run as a cron job, optionally add a string identifier to each request.
```cron
* * * * * curl turpin.cloud?pi | bash
```

- Sync time with the server
- Sleeper mode to preserve battery
- Consider multiple clients/servers on the same machine (don't use global temp?)

# Unique client identifiers
- Hash of `ip addr` or just the MAC address

# Info to report
Each client pushes updates to the server.

- curl ifconfig.co/country
- Run stock scripts and custom per client
- Network speed monitor
- Consolidated view of all procesing available, like a distributed `htop`.
- CPUs available and utilisation
- Ping time
- Audio latency

